package pl.edu.agh.jonczyk.nm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import pl.edu.agh.jonczyk.utils.SpringUtilities;

public class RoutingTablePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Default command used to display system routing configuration
	 */
	private final static String[] ROUTE_COMMAND = { "route", "-n" };

	/**
	 * Columns names
	 */
	String[] columnNames = { "Destination", "Gateway", "Genmask", "Flags",
			"MSS", "Window", "irtt", "Iface", "", "" };

	public static JTable table;
	private Object[][] model;
	private int rows = 0;

	private int row;
	private int column;
	
	RTMouseListener rt_mlistener = new RTMouseListener();

	/*
	 * ##########################################################
	 * default_constructor
	 * ##########################################################
	 */
	@SuppressWarnings("serial")
	public RoutingTablePanel() {

		table = new JTable(getModel(), columnNames) {

			/*
			 * Returning the Class of each column will allow different renderers
			 * to be used based on Class
			 */
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int column) {
				return getValueAt(1, column).getClass();
			}

			public boolean isCellEditable(int row, int column) {
				return false;// This causes all cells to be not editable
			}
		};
		setProperties(table);
		table.addMouseListener(rt_mlistener);
	}

	/*
	 * ###################################################################
	 * getModel()
	 * ###################################################################
	 */
	public Object[][] getModel() {

		String route_cmd = "";
		try {
			route_cmd = execCommand(ROUTE_COMMAND);
		} catch (IOException e) {
			System.out.println();
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		model = new Object[rows][10];
		String[] strings = route_cmd.split("\\s+");

		int ctr = 0;
		for (int k = 0; k < rows; k++) {
			for (int j = 0; j <= 7; j++) {
				model[k][j] = strings[ctr++];
			}
		}
		try {
			// if you want to run it from eclipse remove dots from the beginning
			// of the path to see icons !

			ImageIcon editIcon = new ImageIcon("../resources/img/edit.png");
			ImageIcon delIcon = new ImageIcon("../resources/img/del.png");
			ImageIcon addIcon = new ImageIcon("../resources/img/add.png");

			for (Object[] rows : model) {
				for (int a = 0; a <= 9; a++) {
					if (a == 8) {
						rows[a] = 8;
						rows[a] = editIcon;
					} else if (a == 9) {
						rows[a] = delIcon;
					}
				}
			}
			
			model = Arrays.copyOf(model, (model.length+1));
			model[model.length-1]= new Object[]{"","","","","","","","","",addIcon};
			
		} catch (Exception ex) {
			System.out
					.println("----> Exception while loading application resources");
			ex.printStackTrace();
			Runtime.getRuntime().exit(1);
		}
		return model;
	}
	
	public void addNewRow(){
		
	}
	

	/*
	 * ##########################################################
	 * setProperties()
	 * ##########################################################
	 */
	public void setProperties(JTable table) {

		setName("Routing Table");
		setBorder(new EmptyBorder(15, 30, 30, 30));

		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(true);

		table.setCellSelectionEnabled(true);

		setLayout(new BorderLayout());

		JLabel label = new JLabel(
				"Use buttons to add, remove or modify entries");
		label.setBorder(new EmptyBorder(0, 0, 10, 0));
		add(label, BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		setColumnsWight(table);
	}

	private void setColumnsWight(JTable table) {
		table.getColumnModel().getColumn(0).setPreferredWidth(130);
		table.getColumnModel().getColumn(1).setPreferredWidth(130);
		table.getColumnModel().getColumn(2).setPreferredWidth(120);
		table.getColumnModel().getColumn(3).setPreferredWidth(40);
		table.getColumnModel().getColumn(4).setPreferredWidth(40);
		table.getColumnModel().getColumn(5).setPreferredWidth(60);
		table.getColumnModel().getColumn(6).setPreferredWidth(30);
		table.getColumnModel().getColumn(7).setPreferredWidth(59);
		table.getColumnModel().getColumn(8).setPreferredWidth(30);
		table.getColumnModel().getColumn(9).setPreferredWidth(30);
	}

	/*
	 * ##########################################################
	 * execCommand(String[] command)
	 * ##########################################################
	 */
	public String execCommand(String[] command) throws IOException,
			InterruptedException {

		System.out.println("COMMAND TO EXECUTE:\n");
		for (String sc : command) {
			System.out.print(sc + " ");
		}

		String output = "";
		String s = null;
		rows = 0;

		try {
			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			Process p = Runtime.getRuntime().exec(command);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			// read the output from the command
			System.out
					.println("\nHere is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {

				if (!s.contains("Kernel IP") && !s.contains("Destination")) {
					rows++;
					output += s + " \n";
				}

			}
			// read any errors from the attempted command
			System.out
					.println("\nHere is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			System.out.println("exception occured: ");
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("OUTPUT : ___________ " + output);
		return output;

	}
	

	public void updateAfterChanges(JTable table) {
		DefaultTableModel dm = new DefaultTableModel(getModel(), columnNames);
		table.setModel(dm);
		setColumnsWight(table);
		table.updateUI();

	}

	public void showInputDialog(String addr, String mask, String gw) {

		String[] labels = { "adress: ", "mask: ", "gw: " };
		String[] values = { addr, mask, gw };
		final JTextField[] textFields = new JTextField[3];
		
		int numPairs = labels.length;
		final JDialog dialog = new JDialog();
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);

		JPanel panel = new JPanel(new SpringLayout());
		for (int i = 0; i < numPairs; i++) {
			JLabel l = new JLabel(labels[i], JLabel.TRAILING);
			panel.add(l);
			JTextField textField = new JTextField(10);
			textField.setText(values[i]);
			textField.setSize(60, 10);
			l.setLabelFor(textField);
			panel.add(textField);
			textFields[i]=textField;
		}
		
		JButton invisible1 = new JButton("");
		JButton invisible2 = new JButton("");
		panel.add(invisible1);
		panel.add(invisible2);
		invisible1.setVisible(false);
		invisible2.setVisible(false);

		JButton ok_button = new JButton("OK");
		ok_button.setName("OK");
		JButton cancel_button = new JButton("Cancel");
		panel.add(ok_button);
		panel.add(cancel_button);

		cancel_button.addActionListener(new ActionListener() {
			
		      @Override
		      public void actionPerformed(ActionEvent e) {

		        if (e.getActionCommand().equals("Cancel")) {
		            dialog.dispose();
		        }
		      }
		    });

		ok_button.addActionListener(new ActionListener() {
			
		      @Override
		      public void actionPerformed(ActionEvent e) {

		        if (e.getActionCommand().equals("OK")) {
		           
		            rt_mlistener.addRoute(textFields[0].getText(), textFields[1].getText(), textFields[2].getText(), true);
		            dialog.dispose();
		        }
		      }
		    });

		SpringUtilities.makeCompactGrid(panel, numPairs + 2, 2, // rows, cols
				6, 6, // initX, initY
				6, 6); // xPad, yPad;

		dialog.add(panel);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setSize(200, 200);
		dialog.setVisible(true);
	}

	class RTMouseListener implements MouseListener {

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		public void OKbuttonClicked(){
				editRoute(row, column);
			}

		@Override
		public void mouseClicked(MouseEvent e) {

			row = table.getSelectedRow();
			column = table.getSelectedColumn();

			if (column == 8) {
				editRoute(row, column);
			}
			if (column == 9 && row==table.getModel().getRowCount()-1) {
				showInputDialog("", "", "");
			}
			
			else if (column == 9) {
				deleteRoute(row, column, true);
			}
		}

		public void addRoute(String address, String mask, String gateway,
				boolean updateFlag) {
			try {
				String[] str = { "route", "add", "-net", address, "netmask",
						mask, "gw", gateway };

				execCommand(str);
			} catch (IOException | InterruptedException e) {
				System.out.println("Unable to specify selected route");
				e.printStackTrace();
			} catch (Exception ex) {
				System.out.println("---> Something went wrong :( ");
				ex.printStackTrace();
			}
			if(updateFlag){
				updateAfterChanges(table);
			}
		}

		public void editRoute(int row, int column) {

			showInputDialog((String) table.getValueAt(row, 0), (String) table.getValueAt(row, 2), (String) table.getValueAt(row, 1));
			deleteRoute(row, column, false);
			
			updateAfterChanges(table);
		}

		public void deleteRoute(int row, int column, boolean updateFlag) {

			try {
				String[] str = { "route", "del", "-net",
						(String) table.getValueAt(row, 0), "netmask",
						(String) table.getValueAt(row, 2), "gw",
						(String) table.getValueAt(row, 1) };

				execCommand(str);
			} catch (IOException | InterruptedException e) {
				System.out.println("Unable to specify selected route");
				e.printStackTrace();
			} catch (Exception ex) {
				System.out.println("---> Something went wrong :( ");
				ex.printStackTrace();
			}
			if (updateFlag) {
				updateAfterChanges(table);
			}
		}
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {

		RoutingTablePanel rt = new RoutingTablePanel();
		FrameTester.testFrame(rt, rt.getName());
	}
}
