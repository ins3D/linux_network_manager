package pl.edu.agh.jonczyk.nm;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import javax.swing.JTabbedPane;


public class NetworkManager extends JFrame {

	private static final long serialVersionUID = 1L;


	/**
	 * Launch the application.
	 */
    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
				try {
					NetworkManager frame = new NetworkManager();
					frame.setTitle("Network Manager");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the main frame.
	 */
	public NetworkManager() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	       setSize(900, 600);
	       setLocationRelativeTo(null);
	       setDefaultCloseOperation(EXIT_ON_CLOSE);        
	    
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setBounds(12, 0, 664, 363);
		
		tabbedPane.addTab("<html><body leftmargin=15 topmargin=20 marginwidth=15 marginheight=5>Current connections</body></html>", new Netstat());
		tabbedPane.addTab("<html><body leftmargin=15 topmargin=20 marginwidth=15 marginheight=5>Check connection</body></html>", new PingMe());
		tabbedPane.addTab("<html><body leftmargin=15 topmargin=20 marginwidth=15 marginheight=5>Routes</body></html>", new RoutingTablePanel());

		setContentPane(tabbedPane);
		
	}
}


