package pl.edu.agh.jonczyk.nm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class Netstat extends JPanel {

	/**
	 * Default command used to display current conections -a all 
	 * -t tcp 
	 * -u udp
	 * -p program 
	 * -n numerical
	 */
	private final static String[] NETSTAT_COMMAND = { "netstat", "-atupn" };

	JTable table;
	int rows;
	private Object[][] model;

	/**
	 * Columns names
	 */
	String[] columnNames = { "Proto", "Recv-Q", "Send-Q", "Local Address",
			"Foreign Address", "State", "PID/Program name" };

	/**
	 * Possible connection states
	 */
	String[] conn_states = { "SYN_SEND", "SYN_RECEIVED", "ESTABLISHED",
			"LISTEN", "FIN_WAIT_1", "TIME_WAIT", "CLOSE_WAIT", "FIN_WAIT_2",
			"LAST_ACK", "CLOSED" };

	public Netstat() {

		table = new JTable(getModel(), columnNames) {

			/*
			 * Returning the Class of each column will allow different renderers
			 * to be used based on Class
			 */
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int column) {
				return getValueAt(1, column).getClass();
			}

			public boolean isCellEditable(int row, int column) {
				return false;// This causes all cells to be not editable
			}
		};
		setProperties(table);
	}

	/*
	 * ###################################################################
	 * getModel()
	 * ###################################################################
	 */
	public Object[][] getModel() {

		String command_out = "";
		try {
			command_out = execCommand(NETSTAT_COMMAND);
		} catch (IOException e) {
			System.out.println();
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		model = new Object[rows][7];
		String[] strings = command_out.split("\\r?\\n");

		for (int i = 0; i < model.length; i++) {
			model[i] = strings[i].split("\\s+");

			// if the state is empty
			if (model[i].length == 6) {
				model[i] = Arrays.copyOf(model[i], (model[i].length + 1));
				model[i][6] = model[i][5];
				model[i][5] = "";
			}

			// fix split by space program name
			List<String> rowList = Arrays.asList(conn_states);
			if (!rowList.contains(model[i][5])) {
				model[i] = Arrays.copyOf(model[i], (model[i].length + 1));
				model[i][6] = (model[i][5].toString() + model[i][6]);
				model[i][5] = "";
			}
			if (model[i].length == 8) {
				if (model[i][7] != null)
					model[i][6] = model[i][6].toString() + model[i][7];
			}
		}
		for (int j = 1; j < model.length; j++) {
			for (int k = 0; k < 7; k++) {
				if (model[j][k] == "") {
					model[j][k] = "-";
				}
			}
		}
		return model;
	}

	/*
	 * ##########################################################
	 * execCommand(String[] command)
	 * ##########################################################
	 */
	public String execCommand(String[] command) throws IOException,
			InterruptedException {

		rows = 0;
		System.out.println("\nCOMMAND TO EXECUTE:\n");
		for (String sc : command) {
			System.out.print(sc + " ");
		}

		String output = "";
		String s = null;

		try {
			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			Process p = Runtime.getRuntime().exec(command);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			// read the output from the command
			System.out
					.println("\nHere is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {

				if (!s.contains("Active") && !s.contains("Proto")) {
					rows++;
					output += s + " \n";
				}

			}
			// read any errors from the attempted command
			System.out
					.println("\nHere is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			System.out.println("exception occured: ");
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("OUTPUT : ___________ \n" + output);
		return output;

	}

	/*
	 * ##########################################################
	 * setProperties()
	 * ##########################################################
	 */
	public void setProperties(final JTable table) {

		setName("Active Connections");

		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		setLayout(new BorderLayout());

		setLayout(new BorderLayout());
		JButton button = new JButton("Click to reload");
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = new DefaultTableModel(getModel(), columnNames);
				table.setModel(model);
				table.updateUI();
			}
		});	

		add(button);
	
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		add(button, BorderLayout.NORTH);
		setColumnsWight(table);

	}

	private void setColumnsWight(JTable table) {
		table.getColumnModel().getColumn(0).setWidth(10);
		table.getColumnModel().getColumn(0).setPreferredWidth(10);
		table.getColumnModel().getColumn(1).setPreferredWidth(18);
		table.getColumnModel().getColumn(2).setPreferredWidth(18);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(4).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setPreferredWidth(60);
		table.getColumnModel().getColumn(6).setPreferredWidth(100);

	}
}
