package pl.edu.agh.jonczyk.nm;

import java.awt.Color;
import java.awt.Dimension;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javax.swing.plaf.DimensionUIResource;

@SuppressWarnings("serial")
public class PingMe extends JPanel {

	/**
	 * ping command -c [count of attempts] -w [timeout in seconds]
	 */
	private final static String[] PING_COMMAND = { "ping -c 3 -w 7 " };

	boolean canComm = false;

	public PingMe() {

		this.setName("Ping Panel");

		setLayout(null);

		Insets insets = getInsets();
		ImageIcon runIcon = null;
		ImageIcon checkedIcon = null;
		ImageIcon errorIcon = null;

		try {
			// if you want to run it from eclipse remove dots from the beginning
			// of the path to see icons !
			
			runIcon = new ImageIcon("../resources/img/run.png");
			checkedIcon = new ImageIcon("../resources/img/checked.png");
			errorIcon = new ImageIcon("../resources/img/error.png");

		} catch (Exception ex) {
			System.out
					.println("----> Exception while loading application resources");
			ex.printStackTrace();
			Runtime.getRuntime().exit(1);
		}
		JLabel label = new JLabel("Address to ping");
		final JLabel checked_label = new JLabel("  address succesfully pinged!");
		final JLabel error_label = new JLabel("  No response from this address");

		checked_label.setIcon(checkedIcon);
		error_label.setIcon(errorIcon);

		final JTextField tfield = new JTextField();
		tfield.setPreferredSize(new DimensionUIResource(250, 30));

		JButton runItButton = new JButton("Ping!", runIcon);
		runItButton.setBackground(new Color(15658734));

		Dimension size = checked_label.getPreferredSize();
		checked_label.setBounds(220 + insets.left, 320 + insets.top,
				size.width, size.height);

		size = error_label.getPreferredSize();
		error_label.setBounds(220 + insets.left, 320 + insets.top, size.width,
				size.height);

		error_label.setVisible(false);
		checked_label.setVisible(false);

		add(label);
		add(tfield);
		add(runItButton);
		add(error_label);
		add(checked_label);

		size = label.getPreferredSize();
		label.setBounds(60 + insets.left, 238 + insets.top, size.width,
				size.height);
		size = tfield.getPreferredSize();
		tfield.setBounds(185 + insets.left, 230 + insets.top, size.width,
				size.height);

		runItButton.setPreferredSize(new Dimension(100, 30));
		size = runItButton.getPreferredSize();
		runItButton.setBounds(450 + insets.left, 230 + insets.top, size.width,
				size.height);

		runItButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					execCommand(PING_COMMAND[0] + tfield.getText());

					if (canComm) {
						error_label.setVisible(false);
						checked_label.setVisible(true);
						System.out.println("pinguje");
					} else {
						checked_label.setVisible(false);
						error_label.setVisible(true);
						System.out.println("nie pinguje");
					}

				} catch (IOException | InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});

		tfield.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
			}		
			@Override
			public void mousePressed(MouseEvent arg0) {	
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				error_label.setVisible(false);
				checked_label.setVisible(false);
			}
		});
	}
	
	public String execCommand(String string) throws IOException,
			InterruptedException {

		String output = "";
		String stderr = "";
		String stdout = "";

		canComm = false;

		try {
			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			Process p = Runtime.getRuntime().exec(string);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			// read the output from the command
			System.out
					.println("\nHere is the standard output of the command:\n");
			while ((stdout = stdInput.readLine()) != null) {

				if (stdout.contains("from")) {
					canComm = true;
					System.out.println(stdout);
				}
			}

			// read any errors from the attempted command
			System.out
					.println("\nHere is the standard error of the command (if any):\n");
			while ((stderr = stdError.readLine()) != null) {
				System.out.println(stderr);
			}

		} catch (IOException e) {
			System.out.println("exception occured: ");
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("OUTPUT : ___________ " + output);
		return output;

	}
}