package pl.edu.agh.jonczyk.nm;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class FrameTester {

	public static void testFrame(final JPanel panel, final String frameTitle) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					JFrame frame = new JFrame();
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setLayout(new BorderLayout());
					frame.setTitle(frameTitle + " TEST FRAME");
					frame.setSize(738, 525);	
					frame.add(panel, BorderLayout.CENTER);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
