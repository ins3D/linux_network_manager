You have to run this application with administrative privileges.
To run it you can just use 'runIt.sh' located in src folder.

runIt.sh
######################################################################################################
javac pl/edu/agh/jonczyk/nm/*.java
#su -c '../scripts/addFakeRoutes.sh; chmod -R 777 . ; java pl.edu.agh.jonczyk.nm.NetworkManager'
su -c 'chmod -R 777 . ; java pl.edu.agh.jonczyk.nm.NetworkManager'*
######################################################################################################

You can uncomment line which runs addFakeRoutes.sh to have some more entries in your system configuration
(you may need to modify the script)

addFakeRoutes.sh
######################################################################################################
route add -net 1.0.0.0/16  gw 192.168.0.1
route add -net 2.0.0.0/16  gw 192.168.0.1
route add -net 3.0.0.0/16  gw 192.168.0.1
route add -net 4.0.0.0/16  gw 192.168.0.1
route add -net 5.0.0.0/16  gw 192.168.0.1
######################################################################################################